/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file gev.h
 ***********************************************/
#if !defined(GEV_H)
  #define GEV_H

  #include "realType.h"
  #include <stdint.h>

  void fnGEVP                      (uint16_t unusedButMandatoryParameter);
  void fnGEVL                      (uint16_t unusedButMandatoryParameter);
  void fnGEVR                      (uint16_t unusedButMandatoryParameter);
  void fnGEVI                      (uint16_t unusedButMandatoryParameter);
#endif // !GEV_H
